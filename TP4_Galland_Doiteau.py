import numpy as np
import copy
from scipy import misc

matrice = np.array([[90, 1], [1, 38.5]])


#################################### Exercice 1 ##############################################


def calcul_vecteur_propre(matrice, mu, epsilon):
    vecteur = np.random.rand(1, 2)[0]
    norme = np.linalg.norm(vecteur)
    vecteur_normalise = vecteur / norme

    I = np.identity(matrice.shape[0])
    matrice_intermediaire = np.linalg.inv(matrice - np.dot(mu, I))

    while True:
        nominateur = matrice_intermediaire @ vecteur_normalise
        resultat = nominateur / np.linalg.norm(nominateur)

        if np.linalg.norm(resultat - vecteur_normalise) < epsilon:
            return resultat

        vecteur_normalise = resultat


resultat = calcul_vecteur_propre(matrice, 0, 0.00001)
print("Vecteur propre methode 1: ", resultat)
print("Valeur propre methode 1: ", np.linalg.norm(np.dot(matrice, resultat)))


def calcul_vecteur_propre_1(matrice, epsilon):
    vecteur = np.random.rand(1, 2)[0]
    norme = np.linalg.norm(vecteur)
    vecteur_normalise = vecteur / norme

    lamb = np.transpose(vecteur_normalise) @ matrice @ vecteur_normalise

    while True:
        vecteur_intermediaire = (lamb * matrice) @ vecteur_normalise
        n_vecteur_normalise = vecteur_intermediaire / np.linalg.norm(vecteur_intermediaire)
        n_lamb = np.transpose(n_vecteur_normalise) @ matrice @ n_vecteur_normalise / (
                    np.transpose(n_vecteur_normalise) @ n_vecteur_normalise)

        if abs(n_lamb - lamb) / lamb < epsilon:
            return n_vecteur_normalise, n_lamb

        lamb = n_lamb
        vecteur_normalise = n_vecteur_normalise


resultat_v, resultat_lm = calcul_vecteur_propre_1(matrice, 0.000001)
print("Vecteur propre methode 2: ", resultat_v)
print("Valeur propre methode 2: ", resultat_lm)


#################################### Exercice 2 ##############################################


print("-----------------------------------------------------------")

def newton(poly, x, nbSol, nbIter):
	solutions = []
	for i in range(nbSol):
		solutions.append(newton_step(poly, x, nbIter))
		facteur = np.poly1d([1, -solutions[i]])
		poly = np.polydiv(poly, facteur)[0]
		if(poly.order == 0):
			break
	return solutions

def newton_step(poly, x, nbIter):
	for i in range(nbIter):
		x = x - np.polyval(poly, x) / np.polyval(np.polyder(poly, 1), x)
	return x

poly = np.poly1d([6, -7, 0, 1])
print("Polynome : \n", poly)

solutions = newton(poly, 1, 10, 100)

for i in range(len(solutions)):
	print("Solution ", i, ": " , solutions[i])
	print("Evaluation du polynôme avec cette solution : ", np.polyval(poly, solutions[i]))